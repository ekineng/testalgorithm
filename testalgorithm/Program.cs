﻿using testalgorithm;
// See https://aka.ms/new-console-template for more information
Dictionary<int, int> foodsPropertyDict = new Dictionary<int, int>
{
    { 0, 8 },
    { 1, 10 },
    { 2, 3 },
    { 3, 5 }
};

Dictionary<int, int> sortByNearestFoods = new Dictionary<int, int>();

Coordinate coordinate = new Coordinate(5, 5);

//while (sortByNearestFoods.Count != foodsPropertyDict.Count)
//{
//    foreach (var key in foodsPropertyDict)
//    {
//        if (sortByNearestFoods.Count == 0)
//        {

//        }
//    }
//}
var parentCoordinates = GenerateAllPossibleCoordinate(coordinate);

foreach (Coordinate parent in parentCoordinates)
{
    Node node = new Node();
    node.Parent = parent;

    List<Node> childrenNodes1 = new List<Node>();

    foreach (Coordinate children1 in GenerateAllPossibleCoordinate(parent))
    {
        Node childrenNode1 = new Node();
        childrenNode1.Parent = children1;

        childrenNodes1.Add(childrenNode1);
    }

    node.Children = childrenNodes1;
}

foreach (KeyValuePair<int, int> food in foodsPropertyDict.OrderBy(key => key.Value))
{
    sortByNearestFoods.Add(food.Key, food.Value);
}

List<Coordinate> GenerateAllPossibleCoordinate(Coordinate coordinate)
{
    Coordinate tempCoordinate = null;
    List<Coordinate> allPosibleCoordinates = new List<Coordinate>();

    // up
    tempCoordinate = new Coordinate(coordinate.X, coordinate.Y);
    tempCoordinate.Y++;
    allPosibleCoordinates.Add(tempCoordinate);

    // down
    tempCoordinate = new Coordinate(coordinate.X, coordinate.Y);
    tempCoordinate.Y--;
    allPosibleCoordinates.Add(tempCoordinate);

    // left
    tempCoordinate = new Coordinate(coordinate.X, coordinate.Y);
    tempCoordinate.X--;
    allPosibleCoordinates.Add(tempCoordinate);

    // right
    tempCoordinate = new Coordinate(coordinate.X, coordinate.Y);
    tempCoordinate.X++;
    allPosibleCoordinates.Add(tempCoordinate);

    return allPosibleCoordinates;
}


Console.WriteLine("Hello, World!");


