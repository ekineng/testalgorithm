﻿using System;
namespace testalgorithm
{
	public class Node
	{
		public Coordinate Parent { get; set; }
		public List<Node> Children { get; set; } 
	}
}

